package kurlyk.view.common.controller;

public interface TaskBodyController <T> {

    T getResult();
}
